function Cmdline(console_div, inputlineJQ) {
   var parent = this; //for callbacks
   //output div
   this.cnsl = console_div;
   this.cnsl.innerHTML = '';

   //input text input as JQ object
   this.inputJQ = inputlineJQ;

   this.InterpreterStatus = "begin"; //"begin", "param"
   this.CurrentCommand = '';
   this.CurrentArgument = 0;
   this.Args = [];

   this.cmdlist = new Array();
   this.cmds = new Object();

   this.CommandsToSave = 50;
   this.CommandHistoryIndex = Infinity;
   this.CommandHistory = [];

   //detect special keypresses
   this.inputJQ[0].addEventListener("keydown", function (evt) {
      //return and space
      if (evt.keyCode == 13 || evt.keyCode == 32) {
         evt.preventDefault();
         if (parent.inputJQ.val() == '') {
            return;
         }

         parent.inputJQ.autocomplete('close');
         parent.Interpret(parent.inputJQ.val());
         parent.inputJQ.val('');
         //UP
      } else if (evt.keyCode == 38) {
         evt.preventDefault();
         parent.UP();

         //DOWN
      } else if (evt.keyCode == 40) {
         evt.preventDefault();
         parent.DOWN();
      }

      return false;
   });

   this.Log = function (string, text_from_user) {
      if (typeof (text_from_user) === 'undefined') {
         text_from_user = false;
      }

      var linestart = '> <span style="color:red">';
      var lineend = '</span>';
      if (!text_from_user) {
         linestart = '# ';
         lineend = '';
      }
      var fill = '';
      if (this.cnsl.innerHTML != '') {
         fill = '<br>';
      }
      this.cnsl.innerHTML = this.cnsl.innerHTML + fill + linestart + string + lineend;
      this.cnsl.scrollTop = this.cnsl.scrollHeight;
   }

   this.Interpret = function (input) {
      if (this.InterpreterStatus == "begin" && !this.cmds[input]) {
         var guess = this.Completer(input, this.cmdlist);
         if (this.cmds[guess]) {
            this.Interpret(guess);
         } else {
            this.Log(input, true);
            this.Log("Unknown command: " + input);
         }
      } else if (this.InterpreterStatus == "begin") {
         if (this.CommandHistory.length == 0 || this.CommandHistory[this.CommandHistory.length - 1] != input) {
            this.CommandHistory.push(input);
            this.CommandHistoryIndex = Infinity;
            if (this.CommandHistory.length > this.CommandsToSave) {
               this.CommandHistory.splice(0, this.CommandHistory.length - this.CommandsToSave);
            }
         }

         this.Log(input, true);
         this.InterpreterStatus = "param";
         this.CurrentCommand = input;
         this.CurrentArgument = 0;

         if (this.cmds[this.CurrentCommand].ArgQuery[this.CurrentArgument]) {
            var asd = this.cmds[this.CurrentCommand];
            if (this.cmds[this.CurrentCommand].ArgOptions[this.CurrentArgument]) {
               this.ParamAutocomplete();
            } else {
               this.DisableAutocomplete();
            }
            this.Log(input + ": " + this.cmds[this.CurrentCommand].ArgQuery[this.CurrentArgument]);
         } else {
            this.cmds[this.CurrentCommand].fcn();
            this.InterpreterStatus = "begin";
            this.CurrentCommand = '';
            this.CurrentArgument = 0;
         }
      } else if (this.InterpreterStatus == "param") {
         this.Args[this.CurrentArgument] = this.Completer(input, this.cmds[this.CurrentCommand].ArgOptions[this.CurrentArgument]);
         this.Log(this.Args[this.CurrentArgument], true);
         ++this.CurrentArgument;

         if (this.cmds[this.CurrentCommand].ArgQuery[this.CurrentArgument]) {
            if (this.cmds[this.CurrentCommand].ArgOptions[this.CurrentArgument]) {
               this.ParamAutocomplete();
            } else {
               this.DisableAutocomplete();
            }
            this.Log(input + ": " + this.cmds[this.CurrentCommand].ArgQuery[this.CurrentArgument]);
         } else {
            this.cmds[this.CurrentCommand].fcn(this.Args);
            this.InterpreterStatus = "begin";
            this.CurrentCommand = '';
            this.CurrentArgument = 0;
            this.ResetAutocomplete();
         }

      } else {
         this.Log('Logic error: ' + input);
      }
   }


   this.ParamAutocomplete = function () {
      this.inputJQ.autocomplete('option', 'source', this.cmds[this.CurrentCommand].ArgOptions[this.CurrentArgument]);
      this.inputJQ.autocomplete('option', 'minLength', 0);
      this.inputJQ.autocomplete('enable');
   }
   this.DisableAutocomplete = function () {
      this.inputJQ.autocomplete("disable");
   }
   this.ResetAutocomplete = function () {
      this.inputJQ.autocomplete('option', 'source', this.cmdlist);
      this.inputJQ.autocomplete('option', 'minLength', 2);
      this.inputJQ.autocomplete('enable');
   }

   this.Completer = function (input, values) {
      if (values) {
         for (var i = 0; i < values.length; ++i) {
            if (values[i].indexOf(input) == 0) {
               return values[i];
            }
         }
      }

      return input;
   }

   this.UP = function () {
      if (this.InterpreterStatus == "begin") {
         if (this.CommandHistory.length == 0) {
            return;
         } else if (this.CommandHistoryIndex < 1) {
            this.CommandHistoryIndex = -1;
            this.inputJQ.val('');
         } else if (this.CommandHistoryIndex == Infinity) {
            this.CommandHistoryIndex = this.CommandHistory.length - 1;
            this.inputJQ.val(this.CommandHistory[this.CommandHistoryIndex]);
         }
         else {
            --this.CommandHistoryIndex;
            this.inputJQ.val(this.CommandHistory[this.CommandHistoryIndex]);
         }
      }
   }

   this.DOWN = function () {
      if (this.InterpreterStatus == "begin") {
         if (this.CommandHistory.length == 0) {
            return;
         } else if (this.CommandHistoryIndex >= this.CommandHistory.length - 1) {
            this.CommandHistoryIndex = Infinity;
            this.inputJQ.val('');
         } else {
            ++this.CommandHistoryIndex;
            this.inputJQ.val(this.CommandHistory[this.CommandHistoryIndex]);
         }
      }
   }

   this.AddCommand = function (name, helptext, argquery, argoptions, fcn) {
      if (this.cmds[name]) {
         throw "Command " + name + " already exists.";
      }

      if (arguments.length != 5) {
         throw "Too few arguments: " + name;
      }

      this.cmds[name] = new Object();
      this.cmds[name].fcn = fcn;
      this.cmds[name].help = helptext;
      this.cmds[name].ArgQuery = argquery;
      this.cmds[name].ArgOptions = argoptions;

      CpyArray(Object.getOwnPropertyNames(this.cmds).sort(), this.cmdlist);
   }
}

